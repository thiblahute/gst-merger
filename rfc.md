# [RFC] GStreamer git repositories unification

Hi everyone,

The unification of all GStreamer git repositories into a single one has been
under discussion for a while among developers. A first version is now ready
and it is time to open the discussion with our users.

Technical details and discussion can be found here:
https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/474

Unified repository being tested:
https://gitlab.freedesktop.org/thiblahute/gstreamer/-/tree/monorepo_simple

## What is it?

Merges all GStreamer modules into a single git repository: gstreamer,
gst-plugins-base, gst-plugins-good, gst-plugins-bad, gst-plugins-ugly,
gstreamer-vaapi, gst-omx, gst-libav, gst-ci, gst-rtsp-server,
gst-editing-services, gst-devtools, gst-docs, gst-examples,
gst-integration-testsuites, gst-python, gstreamer-sharp, gstreamer-rs,
gst-plugins-rs and gst-build.

The full git history of all those modules is merged into the existing
`gstreamer` git repo, every commit hash is preserved. At the moment only git
repositories are considered to be merged, not release tarballs. Each GStreamer
component can still be built separately.

Note that only current master branch is being unified, stable branches will be
kept in their own repositories.

## Why?

- Cross repositories changes are currently painful because they need to
  coordinate Merge Requests in multiple repositories. It is easier for
  developers and reviewers to have all the code in a single MR.
- Simplification of the CI infrastructure. No more manifest telling which branch
  in which user fork needs to be pulled together. The CI can checkout and test a
  single branch. This also makes replicating a similar setup downstream much
  simpler.
- Makes bisecting regressions much simpler. Once the unification point will be
  behind us, bisecting GStreamer will be a simple matter of running `git bisect`
  without worrying to keep multiple repositories in sync at each step.
- Simplifies the release process as it can be prepared in a single MR instead of
  bumping version and tagging each module separately.
- Makes easier to move files across modules, such as moving plugins from -bad to
  -good or -ugly.

An extensive list of pros and cons can be found here:
https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/474#pros-and-cons-of-a-monorepo

## What does it mean for distributions?

Since release tarballs are preserved, nothing should change for packagers.

## What does it mean for contributors?

- The unified gstreamer repository will have the layout of gst-build.
- All merge requests targeting master branch will have to be made again the
  main gstreamer repository.
- Existing merge requests on other repositories (e.g. gst-plugins-good) will
  have to be resubmitted on the main repo. A simple rebase or cherry-pick usually
  works.
- All GitLab issues will be moved to the main repository.


We have prepared a [execution plan] so you can see how we currently think about
making it happen. Although this is a Request For Comment so that the broader
community can help us execute it as well as possible.


[execution plan]: https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/474#plan