#!/usr/bin/env python3

import os
import gitlab

GITLAB_API_TOKEN = os.environ['GITLAB_API_TOKEN']


def main():
    print("Logging into gitlab and fetching a list of projects...")
    gl = gitlab.Gitlab('https://gitlab.freedesktop.org', private_token=GITLAB_API_TOKEN)
    projects = gl.projects.list(membership=1, all=True)

    print(f"Here are the projects we found ")
    for i, project in enumerate(projects):
        print(f"    {i+1}. {project.path_with_namespace}")

    from_project_number = input(f"\nWhich project do you want to move issues FROM? Enter  1 - {len(projects)}: ")
    to_project_number = input(f"Which project do you want to move issues TO? Enter  1 - {len(projects)}: ")
    from_project = projects[int(from_project_number.strip()) - 1]
    to_project = projects[int(to_project_number.strip()) - 1]

    print(f'Fetching issues for {from_project.path_with_namespace} (this can take a while)...')
    issues = from_project.issues.list(all=True)
    issues = sorted(issues, key=lambda i: i.iid)
    issues = [i for i in issues if i.state.lower() == "opened"]

    print(f'Fetching labels for {from_project.path_with_namespace}...')
    labels = from_project.labels.list(all=True)
    existing_labels = to_project.labels.list(all=True)

    print(f'Will move {len(issues)} issues from {from_project.path_with_namespace} to {to_project.path_with_namespace}')
    print(f'Will also create {len(labels)} labels in project {to_project.path_with_namespace}')
    cont = input('Continue [y/n]? ')
    if cont.strip().lower() != 'y':
        return

    print('Creating labels')
    for label in labels:
        if [l for l in existing_labels if l.name == label.name]:
            print(f'Label {label.name} already exists in {to_project.path_with_namespace}. Skipping.')
            continue

        to_project.labels.create({
            'name': label.name,
            'color': label.color,
            'description': label.description,
            'priority': label.priority,
        })
        print(f'    Created {label.name}')

    print(f'Starting moving...')
    for issue in issues:
        print(f'   Moving issue {issue.iid}: {issue.title}... ', end='', flush=True)

        while True:
            try:
                issue.move(to_project.id)
            except OSError as e:
                print(f'\nError encountered ({e}). Retrying... ', end='', flush=True)
            except gitlab.GitlabUpdateError as e:
                if e.response_code == 400:
                    print(
                        f"\nUpdate error, insufficient permissions. Skipping this issue as this "
                        f"normally means the issue has already been moved."
                    )
                    break
                else:
                    raise
            else:
                break

        print('done')

    print('Move complete!')


if __name__ == '__main__':
    main()
