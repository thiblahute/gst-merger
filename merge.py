#! /usr/bin/python3

import os
import argparse
import subprocess
from subprocess import call
from subprocess import check_output

global OPTIONS

def move_omx(repo, repodir):
  call(['rm', '-Rf', 'common/'])
  safe_call(['git', 'mv', 'omx', 'tmpomx'])
  safe_call(['git', 'commit', '--amend', '-n', '--no-edit'])
  git_mv(repo, repodir)
  safe_call(['git', 'mv', 'tmpomx', 'subprojects/omx/omx/'])
  safe_call(['git', 'commit', '--amend', '-n', '--no-edit'])

def do_nothing(*args):
  return

REPOS = {
  'gst-plugins-base': {
    'dirname': 'subprojects/gst-plugins-base',
  },
  'gst-plugins-good': {
     'dirname': 'subprojects/gst-plugins-good'
   },
   'gst-plugins-bad': {
     'dirname': 'subprojects/gst-plugins-bad'
   },
   'gst-plugins-ugly': {
     'dirname': 'subprojects/gst-plugins-ugly'
   },
   'gstreamer-vaapi': {
     'dirname': 'subprojects/gstreamer-vaapi'
   },
   'gst-omx': {
     'dirname': 'subprojects/gst-omx',
    #  'move-func': move_omx,
   },
   'gst-libav': {
     'dirname': 'subprojects/gst-libav',
   },
   'gst-ci': {
     'dirname': 'subprojects/gst-ci',
   },
   # libs
   'gst-rtsp-server': {
     'dirname': 'subprojects/gst-rtsp-server',
   },
   'gst-editing-services': {
     'dirname': 'subprojects/gst-editing-services',
   },
   'gst-devtools': {
     'dirname': 'subprojects/gst-devtools',
   },
   'gst-docs': {
     'dirname': 'subprojects/gst-docs',
   },
   'gst-examples': {
     'dirname': 'subprojects/gst-examples',
   },
   'gst-integration-testsuites': {
     'dirname': 'subprojects/gst-integration-testsuites',
   },
   # bindings
   'gst-python': {
     'dirname': 'subprojects/gst-python/',
   },
   'gstreamer-sharp': {
     'dirname': 'subprojects/gstreamer-sharp',
   },
   'gstreamer-rs': {
     'dirname': 'subprojects/gstreamer-rs',
   },
   'gst-plugins-rs': {
     'dirname': 'subprojects/gst-plugins-rs',
   },
   'gst-build': {
     'dirname': 'gst-build',
     'move-func': do_nothing,
   },
}

TOPLEVEL_DIRS = ['.git', 'subprojects']
BASE_PATH = os.path.abspath(os.curdir)

def safe_call(args, cwd=None, silent=False, shell=False):
  if not silent:
    print('%s $ "%s"' % (os.path.basename(os.path.abspath(cwd if cwd else os.curdir)), '" "'.join(args) if not shell else args))
  try:
    return check_output(args, stderr=subprocess.STDOUT, cwd=cwd, shell=shell).decode('utf-8')
  except subprocess.CalledProcessError as e:
    print(e.stdout.decode('utf-8'))
    if "COPYING deleted in HEAD" in e.stdout.decode('utf-8'):
      safe_call(['git', 'add', 'COPYING'])
      call(['git', 'commit', '-a', "-n"])
    else:
      import ipdb; ipdb.set_trace()
    # raise e
  except:
    import ipdb; ipdb.set_trace()

def is_toplevel(repodir, f):
  return f in TOPLEVEL_DIRS

def get_merged_files(p=False):
  # Extract list of files added from previous merged commit
  stats = safe_call(['git', 'diff', '--name-only', 'HEAD^..HEAD'])
  if p:
    print(stats)
  res= [f for f in stats.split('\n') if os.path.exists(f)]
  return res

def git_mv(repo, repodir, all=False):
  call(['rm', '-Rf', 'common/'])
  os.makedirs(repodir, exist_ok=True)
  if not all:
    to_move = get_merged_files()
  else:
    to_move = [f for f in os.listdir(os.curdir) if not is_toplevel(repodir, f)]
  if [t for t in to_move if '..' in t]:
    to_move = get_merged_files(True)
    import ipdb; ipdb.set_trace()
  for f in to_move:
    dirname = os.path.dirname(f)
    if dirname:
      _dir = os.path.join(repodir, dirname)
      os.makedirs(_dir, exist_ok=True)
    else:
      _dir = repodir

    try:
      safe_call(['git', 'mv', f, _dir + '/'])
    except:
      import ipdb; ipdb.set_trace()
  safe_call(['git', 'commit', '--no-edit', '-n', '-m', 'Move files from %s into  the "%s/" subdir' % (repo, repodir)])
  print(safe_call(['ls']))

def remote_add(repo):
  remotes = safe_call(['git', 'remote', '-v'], silent=True)
  if os.path.exists(BASE_PATH + '/../' + repo) and 'integration-testsuites' not in repo:
    remote = BASE_PATH + '/../' + repo
    if remote in remotes:
      print('Remote %s already exists' % remote)
      return False
    # safe_call(['git', 'fetch', 'origin'], cwd=remote)
    safe_call(['git', 'remote', 'add', repo, remote])
  else:
    remote = 'https://gitlab.freedesktop.org/gstreamer/' + repo
    if remote in remotes:
      print('Remote %s already exists' % remote)
      return False
    safe_call(['git', 'remote', 'add', repo, remote])

  return True

def clone_core_if_needed():
  repo = 'gstreamer'
  repo_name = 'gstreamer_merged'
  if not os.path.exists(repo_name):
    if os.path.exists(BASE_PATH + repo):
      safe_call(['cp', '-r', BASE_PATH + repo, repo_name])
      # safe_call(['git', 'fetch', 'origin'], cwd=repo_name)
      call(['git', 'checkout', 'one_repo'], cwd=repo_name)
      safe_call(['git', 'reset', '--hard', 'origin/master'], cwd=repo_name)
      safe_call(['git', 'clean', '-fdx'], cwd=repo_name)
      safe_call(['git', 'config', 'lfs.url', 'https://gitlab.freedesktop.org/gstreamer/gst-integration-testsuites.git/info/lfs'], cwd=repo_name)
    else:
      safe_call(['git', 'clone', 'https://gitlab.freedesktop.org/gstreamer/' + repo, repo_name])
  else:
    print('GStreamer core already in %s')

  os.chdir(repo_name)

def repo_is_merged(repo, repodir):
  return remote_add(repo) is False and os.path.exists(repodir)

def merge_repos_if_needed():
  safe_call(['mkdir', '-p', 'subprojects/'])
  if not repo_is_merged('gstreamer', 'gstreamer'):
    git_mv('gstreamer', 'subprojects/gstreamer', all=True)
  else:
    print(' -> INFO: gstreamer gstreamer ALREADY MERGED')

  for repo, data in REPOS.items():
    repodir = data['dirname']
    if repodir is None:
      continue
    if remote_add(repo) is False and os.path.exists(repodir):
      print(' -> INFO: %s ALREADY MERGED into %s' % (repo, repodir))
      continue
    safe_call(['git', 'remote', 'update', repo])
    try:
      safe_call(['git', 'merge', '--no-edit', '--allow-unrelated-histories', repo + '/master', '-m', 'Merging %s' % repo])
    except subprocess.CalledProcessError as e:
      # Workaround stupid merge issue in -bad
      if "COPYING deleted in HEAD" in e.stdout.decode('utf-8'):
        safe_call(['git', 'add', 'COPYING'])
        call(['git', 'commit', '-a', "-n"])
      else:
        raise e
    call(['git', 'commit', '-a', "-n", "--amend"])
    data.get('move-func', git_mv)(repo, repodir)
    print(safe_call(['git', 'status']))


def move_files_from_core():
  call(['git', 'mv', 'src/gstreamer/.gitignore', '.'])
  call(['git', 'mv', 'src/gstreamer/hooks', '.'])
  call(['git', 'add', '.gitignore'])
  call(['git', 'add', 'hooks/'])

def setup_meson():
  safe_call('git mv subprojects/devtools/devenv/subprojects/*.wrap subprojects/', shell=True)
  safe_call("git mv subprojects/devtools/devenv/meson.build subprojects/devtools/devenv/meson_options.txt .", shell=True)
  safe_call("git mv subprojects/devtools/devenv/gstreamer-full-default.map .", shell=True)
  safe_call("git commit -a -m 'WIP'", shell=True)
  # safe_call("git sed \"s/'gstreamer'/'core'/g\"", shell=True)
  # safe_call("git sed \"s/'gst-plugins-base'/'base'/g\"", shell=True)
  # safe_call("git sed \"s/'gst-plugins-bad'/'bad'/g\"", shell=True)
  # safe_call("git sed \"s/'gst-devtools'/'base'/g\"", shell=True)
  safe_call("git add `find -name meson.build|grep -v pkgconfig |grep -v check`", shell=True)

def merge_options():
  call(['git', 'rm', 'meson_options.txt', '-f'])
  call(['git', 'checkout', '--', 'gstreamer/meson_options.txt'])
  call(['git', 'mv', 'gstreamer/meson_options.txt', '.'])
  for repo, data in REPOS.items():
    repodir = data['dirname']
    optionfile = os.path.join(repodir, 'meson_options.txt')
    if os.path.exists(optionfile):
      with open('meson_options.txt', 'a') as f:
        with open(optionfile, 'r') as repo_options:
          f.write(repo_options.read())
      safe_call(['git', 'rm', optionfile])
  safe_call(['git', 'add', 'meson_options.txt'])


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-u')

  global OPTIONS
  OPTIONS = parser.parse_args()
  clone_core_if_needed()
  peditor = os.environ.get('GIT_EDITOR')
  os.environ['GIT_EDITOR'] = 'true'

  merge_repos_if_needed()
  if peditor:
    os.environ['GIT_EDITOR'] = peditor
  else:
    del os.environ['GIT_EDITOR']